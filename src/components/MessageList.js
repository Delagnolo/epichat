import React from "react";
import { Timeline } from "antd";

const MessageList = ({ messages }) => (
  <Timeline>
    {messages.map((msg) => (
      <Timeline.Item key={msg.id}>
        {msg.text}
      </Timeline.Item>
    ))}
  </Timeline>
);

export default MessageList;
